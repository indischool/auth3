<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexPageTest extends TestCase
{
    /** @test */
    public function it_redirect_to_login_page_when_a_user_is_not_logged_in()
    {
        if (env('APP_ENV') === 'prod' && $_SESSION['is_logged'] !== null && $_SESSION['is_logged'] == true) {
            $this->get('/')->assertRedirect('/login');
        }

        $this->get('/')->assertStatus(200);
    }
}
