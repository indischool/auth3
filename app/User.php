<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    protected $table = 'indi_member';

    protected $primaryKey = 'member_srl';

    protected $guarded = [];

    public $timestamps = false;


    /**
     *  이름과 학교명 정보 업데이트
     *
     * @param $data
     */
    public static function updateUserInfoByAuthData($data)
    {
        $user = self::find(config('user')->member_srl);

        if ($user) {
            $user->user_name = $data->name;
            $extra_vars = unserialize($user->extra_vars);
            $extra_vars->mem_school = $data->school_name;
            $user->extra_vars = serialize($extra_vars);
            $user->save();
        }
    }

    /**
     * 로그인 사용자가 인디벗(교사인증) 그룹을 가지고 있고 인증 테이블에도 정보가 있는지 확인하여 반환
     *
     * @param $member_srl
     * @return bool
     */
    public static function isAuthorized()
    {
        $member_srl = config('user')->member_srl;
        $hasGroup = false;
        $hasIndiauth = false;

        $groups = MemberGroup::where('site_srl', '0')
            ->where('member_srl', $member_srl)->get()->all();

        foreach ($groups as $group) {
            if ($group->group_srl == config('indiauth.authorized_group_srl')) {
                $hasGroup = true;
            }
        }

        $hasIndiauth = Indiauth::find($member_srl);

        return ($hasGroup && $hasIndiauth)? true : false;
    }

    /**
     * member_srl을 통해 인증 날짜, 인증 만료 일자, 재인증까지 남은 날짜를 Carbon 객체로 반환
     *
     * @param $user
     * @param $authInfo
     */
    public static function getAuthDate($authInfo)
    {
        $member_srl = config('user')->member_srl;

        $indiauth = Indiauth::find($member_srl);

        $y = substr($indiauth->authdate, 0, 4);
        $m = substr($indiauth->authdate, 4, 2);
        $d = substr($indiauth->authdate, 6, 2);

        $authDate = Carbon::create($y, $m, $d, 0, 0, 0);
        $expireDate = clone $authDate;
        $expireDate = $expireDate->addYear(config('indiauth.period_of_validity'));

        $authInfo->authDate = $authDate;
        $authInfo->expireDate = $expireDate;
        $authInfo->diffInDays = $expireDate->diffInDays(Carbon::now());
    }
}
