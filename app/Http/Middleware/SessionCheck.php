<?php

namespace App\Http\Middleware;

use Closure;

class SessionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // production
        if (env('APP_ENV') === 'prod') {
            if (!isset($_SESSION)) {
                @session_start();
            }

            // 로그인이 되어 있을 경우
            if (isset($_SESSION['is_logged'])) {
                if ($_SESSION['is_logged'] == true) {
                    // member_srl 값을 config에 설정.
                    config(['user' => \App\User::where('member_srl', $_SESSION['member_srl'])->get()->first()]);
                } else {
                    return redirect('/login');
                }
            } else {
                // 로그인이 되어 있지 않을 경우 로그인 안내 메시지 출력
                return redirect('/login');
            }
        }

        // 개발 환경
        else {
            // nala2sky
            config(['user' => \App\User::where('member_srl', '1459520')->get()->first()]);

            // jdssem
//            config(['user' => \App\User::where('member_srl', '8942150')->get()->first()]);
        }

        return $next($request);
    }
}
