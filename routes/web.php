<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'PageController@login');
Route::get('outdated-browser', 'PageController@outdatedBrowser');

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'SimpleAuthController@index');
    Route::get('howto', 'PageController@howto');
    Route::get('faq', 'PageController@faq');
    Route::get('success', 'PageController@success');

    // 간편인증
    Route::get('simple/step1', 'SimpleAuthController@simpleStep1');
    Route::post('simple/step1', 'SimpleAuthController@compareID');
    Route::post('simple/step2', 'SimpleAuthController@privacy');
    Route::post('simple/step3', 'SimpleAuthController@howto');
    Route::post('simple/step4', 'SimpleAuthController@store');
});
