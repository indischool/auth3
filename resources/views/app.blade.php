<!DOCTYPE html>
<html lang="ko" class="has-navbar-fixed-top">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css">
    <title>인디스쿨 인증센터</title>
</head>
<body class="has-background-white-ter">
    <nav class="navbar is-fixed-top">
        <div class="navbar-brand">
            <a class="navbar-item" href="http://auth.indischool.com">
            <img src="/img/indiauth_logo.png" alt="인디스쿨 인증센터" width="187" height="28">
            </a>
            <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
            <span></span>
            <span></span>
            <span></span>
            </div>
        </div>
        
        <div id="navbarExampleTransparentExample" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="#">
                    간편인증
                </a>
                <a class="navbar-item" href="#">
                    서류인증
                </a>
                <a class="navbar-item" href="#">
                    자주 묻는 질문
                </a>
                <a class="navbar-item" href="#">
                    인디스쿨 돌아가기
                </a>
            </div>
        </div>
    </nav>


    <section class="section">
        <div class="container">
            <div class="columns is-centered">
                <div class="column is-one-third">
                        <div class="card">
                                <div class="card-content">
                                  <div class="media">
                                    <div class="media-left">
                                      <figure class="image is-48x48">
                                        <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                      </figure>
                                    </div>
                                    <div class="media-content">
                                      <p class="title is-4">John Smith</p>
                                      <p class="subtitle is-6">@johnsmith</p>
                                    </div>
                                  </div>
                              
                                  <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                  </div>
                                </div>
                              </div>
                </div>
                <div class="column">
                        <div class="card">
                                <div class="card-image">
                                  <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                                  </figure>
                                </div>
                                <div class="card-content">
                                  <div class="media">
                                    <div class="media-left">
                                      <figure class="image is-48x48">
                                        <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                      </figure>
                                    </div>
                                    <div class="media-content">
                                      <p class="title is-4">John Smith</p>
                                      <p class="subtitle is-6">@johnsmith</p>
                                    </div>
                                  </div>
                              
                                  <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                  </div>
                                </div>
                              </div>
                </div>
                <div class="column">
                        <div class="card">
                                <div class="card-image">
                                  <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                                  </figure>
                                </div>
                                <div class="card-content">
                                  <div class="media">
                                    <div class="media-left">
                                      <figure class="image is-48x48">
                                        <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                      </figure>
                                    </div>
                                    <div class="media-content">
                                      <p class="title is-4">John Smith</p>
                                      <p class="subtitle is-6">@johnsmith</p>
                                    </div>
                                  </div>
                              
                                  <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                  </div>
                                </div>
                              </div>
                </div>
            </div>
        </div>
    </section>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
</body>
</html>